<?php

namespace aminkt\payment\exceptions;

/**
 * Class ConnectionException
 * @author Amin Keshavarz <ak_1596@yahoo.com>
 * @package aminkt\payment\exceptions
 */
class ConnectionException extends \RuntimeException
{

}