<?php

namespace aminkt\payment\exceptions;

/**
 * Class VerifyPaymentException
 * @author Amin Keshavarz <ak_1596@yahoo.com>
 * @package aminkt\payment\exceptions
 */
class VerifyPaymentException extends \RuntimeException
{

}