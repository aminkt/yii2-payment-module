<?php

namespace aminkt\payment\exceptions;

/**
 * Class SecurityException
 * @author Amin Keshavarz <ak_1596@yahoo.com>
 * @package aminkt\payment\exceptions
 */
class SecurityException extends \RuntimeException
{

}